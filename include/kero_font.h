#ifndef FONT_H
#define FONT_H

#ifdef __cplusplus
extern "C"{
#endif
    
#include "kero_std.h"
#include "kero_sprite.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
    
    typedef struct{
        ksprite_t characters[256];
        int spacingh[256];
        int height;
    } kfont_t;
    
    /* Pass a folder path which contains the font files
font.png
metadata.txt*/
    bool KF_Load(kfont_t* font, char* folder){
        ksprite_t font_spr;
        char filepath[512];
        int w16th, h16th;
        FILE *file;
        int *spacing;
        char *line;
        size_t line_size = 256;
        int x, y;
        char c;
        
        sprintf(filepath, "%s/font.png", folder);
        if(!KS_Load(&font_spr, filepath)) {
            fprintf(stderr, "Failed to load font: %s\n", filepath);
            return false;
        }
        w16th = font_spr.w / 16;
        h16th = font_spr.h / 16;
        font->height = h16th + 1;
        for(y = 0; y < 16; y++){
            for(x = 0; x < 16; x++){
                KS_CreateFromSprite(&font->characters[y*16+x], &font_spr, x*w16th, y*h16th, (x+1)*w16th, (y+1)*h16th);
            }
        }
        
        spacing = font->spacingh;
        for(x = 0; x < 256; ++x) *(spacing++) = w16th;
        
        sprintf(filepath, "%s/metadata.txt", folder);
        if((file = fopen(filepath, "r")) != NULL) {
            line = malloc(256);
            if(getline((char**)&line, &line_size, file) > 0) {
                if((x = FindString(line, line_size, "height", 6)) != -1) {
                    font->height = atoi(&line[x+6]);
                }
            }
            while(!feof(file) && getline((char**)&line, &line_size, file) > 0) {
                c = line[0];
                font->spacingh[(int)c] = atoi(&line[2]);
            }
            free(line);
            fclose(file);
        }
        return true;
    }
    
    void KF_Draw(kfont_t* font, ksprite_t* target, int x, int y, char* text){
        int xoffset = 0;
        for(int unsigned i = 0; text[i] != '\0'; i++){
            if(text[i] == '\n'){
                y += font->height;
                xoffset = 0;
            } else{
                KS_Blit(&font->characters[(int)text[i]], target, x+xoffset, y);
                xoffset += font->spacingh[(int)text[i]];
            }
        }
    }
    
    void KF_DrawAlpha10(kfont_t* font, ksprite_t* target, int x, int y, char* text){
        int xoffset = 0;
        for(int unsigned i = 0; text[i] != '\0'; i++){
            if(text[i] == '\n'){
                y += font->height;
                xoffset = -(i+1)*16;
            } else{
                KS_BlitAlpha10(&font->characters[(int)text[i]], target, x+xoffset, y);
                xoffset += font->spacingh[(int)text[i]];
            }
        }
    }
    
    void KF_DrawBlend(kfont_t* font, ksprite_t* target, int x, int y, char* text){
        int xoffset = 0;
        for(int unsigned i = 0; text[i] != '\0'; i++){
            if(text[i] == '\n'){
                y += font->height;
                xoffset = 0;
            } else{
                KS_BlitBlend(&font->characters[(int)text[i]], target, x+xoffset, y);
                xoffset += font->spacingh[(int)text[i]];
            }
        }
    }
    
    void KF_DrawColored(kfont_t* font, ksprite_t* target, int x, int y, char* text, uint32_t colour){
        int xoffset = 0;
        for(int unsigned i = 0; text[i] != '\0'; i++){
            if(text[i] == '\n'){
                y += font->height;
                xoffset = 0;
            } else{
                KS_BlitColoredAlpha10(&font->characters[(int)text[i]], target, x+xoffset, y, 0, 0, colour);
                xoffset += font->spacingh[(int)text[i]];
            }
        }
    }
    
    void KF_DrawBlendAlphaBlend(kfont_t* font, ksprite_t* target, int x, int y, char* text, uint8_t alpha){
        int xoffset = 0;
        for(int unsigned i = 0; text[i] != '\0'; i++){
            if(text[i] == '\n'){
                y += font->height;
                xoffset = 0;
            } else{
                KS_BlitBlendAlphaBlend(&font->characters[(int)text[i]], target, x+xoffset, y, alpha);
                xoffset += font->spacingh[(int)text[i]];
            }
        }
    }
    
#ifdef __cplusplus
}
#endif

#endif