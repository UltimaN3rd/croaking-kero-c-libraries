#ifndef KERO_STD_H

#if __cplusplus__
extern "C" {
#endif
    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
    
    typedef struct {
        union {
            uint32_t length;
            uint32_t len;
            uint32_t l;
        };
        union {
            char *data;
            char *d;
            char *str;
            char *s;
            char *text;
            char *t;
        };
    } kstring_t;
    
    inline bool KStd_StringInit(kstring_t *string, const char *const text) {
        string->length = strlen(text);
        string->text = malloc(string->length);
        if(!string->text) {
            return false;
        }
        strcpy(string->text, text);
        return true;
    }
    
    inline void KStd_StringFree(kstring_t *string) {
        if(string->text) {
            free(string->text);
        }
    }
    
#if __WIN32
#define GETLINE_BLOCK_SIZE 1024
    int getline(char **lineptr, size_t *n, FILE *stream) {
        if(*lineptr == NULL) { // Need to allocate line
            *lineptr = malloc(GETLINE_BLOCK_SIZE);
            if(*lineptr == NULL) return -1;
            *n = GETLINE_BLOCK_SIZE;
        } // Else line is already allocated. *n = allocated line size, else error
        if(n == NULL || *n == 0) return -1;
        unsigned char c = ' ';
        int i = 0;
        while(c != '\n' && c != '\0' && !feof(stream)) {
            if((c = fgetc(stream)) == EOF) {
                printf("getline: EOF or error\n");
                break;
            }
            (*lineptr)[i] = c;
            if(++i == *n) {
                *n += *n;
                *lineptr = realloc(*lineptr, *n);
                if(*lineptr == NULL) {
                    printf("getline: Failed to realloc *lineptr\n");
                    return -1;
                }
            }
        }
        (*lineptr)[i] = '\0';
        return *n;
    }
#endif // __WIN32
    
    int FindString(const char *source, int source_length, const char *find, int find_length) {
        //printf("FindString(\"%s\" \"%s\")\n", source, find);
        if(source_length == 0) source_length = strlen(source);
        if(find_length == 0) find_length = strlen(find);
        if(source_length == 0 || find_length == 0) return -1;
        int match;
        for(int c = 0; c < source_length - find_length; ++c) {
            if(source[c] == find[0]) {
                for(match = 1; match < find_length; ++match) {
                    if(source[c + match] != find[match]) {
                        break;
                    }
                }
                if(match == find_length) { // match found
                    //printf("Found %d\n", c);
                    return c;
                }
            }
        }
        return -1;
    }
    
    typedef enum {
        KSTD_SUCCESS = 0, KSTD_ERROR_FILENOTFOUND, KSTD_ERROR_FILESEEK, KSTD_ERROR_FILESIZE, KSTD_ERROR_MALLOC, KSTD_ERROR_FILEREAD, KSTD_ERROR_BUFFERRSIZE, KSTD_ERROR_BUFFERNULL
    } kstd_error_t;
    kstd_error_t KStd_ReadFile(const char *const filepath, uint8_t **data, uint32_t *byte_count) {
        FILE *file = fopen(filepath, "rb");
        if(!file) {
            return KSTD_ERROR_FILENOTFOUND;
        }
        if(fseek(file, 0, SEEK_END) != 0) {
            fclose(file);
            return KSTD_ERROR_FILESEEK;
        }
        *byte_count = ftell(file);
        if(*byte_count <= 0) {
            fclose(file);
            return KSTD_ERROR_FILESIZE;
        }
        rewind(file);
        *data = malloc(*byte_count + 1);
        if(!*data) {
            fclose(file);
            return KSTD_ERROR_MALLOC;
        }
        if(fread(*data, 1, *byte_count, file) < *byte_count) {
            fclose(file);
            free(*data);
            return KSTD_ERROR_FILEREAD;
        }
        fclose(file);
        data[*byte_count] = 0;
        return KSTD_SUCCESS;
    }
    
    kstd_error_t KStd_ReadFileFixedBuffer(const char *const filepath, char *data, uint32_t buffer_size, uint32_t *read_bytes) {
        uint32_t byte_count;
        FILE *file = fopen(filepath, "rb");
        if(!file) {
            return KSTD_ERROR_FILENOTFOUND;
        }
        if(fseek(file, 0, SEEK_END) != 0) {
            fclose(file);
            return KSTD_ERROR_FILESEEK;
        }
        byte_count = ftell(file);
        if(byte_count <= 0) {
            fclose(file);
            return KSTD_ERROR_FILESIZE;
        }
        else if(byte_count > buffer_size) {
            fclose(file);
            return KSTD_ERROR_BUFFERRSIZE;
        }
        rewind(file);
        if(!data) {
            fclose(file);
            return KSTD_ERROR_BUFFERNULL;
        }
        if(fread(data, 1, byte_count, file) < byte_count) {
            fclose(file);
            return KSTD_ERROR_FILEREAD;
        }
        fclose(file);
        if(read_bytes) *read_bytes = byte_count;
        return KSTD_SUCCESS;
    }
    
#ifndef KERO_DYNAMIC_ARRAY_NO_SHORT_NAMES
#define KDA_Init KeroDynamicArray_Init
#define KDA_Free KeroDynamicArray_Free
#define KDA_Push KeroDynamicArray_Push
#define KDA_Size KeroDynamicArray_Size
#define KDA_Top KeroDynamicArray_Top
#define KDA_Shrink KeroDynamicArray_Shrink
#define KDA_Resize KeroDynamicArray_Resize
#endif
    
    void FuncNil() {}
    
#define KeroDynamicArray_Size(array) (\
((uint32_t*)(array))[-2] \
)
    
#define KeroDynamicArray_Top(array) (\
((uint32_t*)(array))[-1] \
)
    
#define KeroDynamicArray_Init(array, start_size) (\
*(void **)&(array) = (uint32_t*)malloc(sizeof(*(array)) * start_size + sizeof(uint32_t) * 2) + 2, \
KeroDynamicArray_Size(array) = start_size, \
KeroDynamicArray_Top(array) = 0, \
(array) = (array) \
)
    
#define KeroDynamicArray_Free(array) (\
free(&((uint32_t *)(array))[-2]) \
)
    
#define KeroDynamicArray_Push(array, data) (\
( KeroDynamicArray_Size(array) <= KeroDynamicArray_Top(array) ? \
KeroDynamicArray_Size(array) *= 2, *(void **)&(array) = (uint32_t*)realloc(&(*(uint32_t **)&(array))[-2], sizeof(*(array)) * KeroDynamicArray_Size(array) + sizeof(uint32_t) * 2) + 2 : 0), \
(array) != NULL ? \
((array)[KeroDynamicArray_Top(array)++] = (data)), (array) : \
NULL \
)
    
#define KeroDynamicArray_Shrink(array) (\
( KeroDynamicArray_Size(array) > KeroDynamicArray_Top(array) ? \
KeroDynamicArray_Size(array) = KeroDynamicArray_Top(array), *(void **)&(array) = (uint32_t*)realloc(&(*(uint32_t **)&(array))[-2], sizeof(*(array)) * KeroDynamicArray_Size(array) + sizeof(uint32_t) * 2) + 2 : 0), \
(array) = (array) \
)
    
#define KeroDynamicArray_Resize(array, size) (\
( KeroDynamicArray_Size(array) = (size), *(void **)&(array) = (uint32_t*)realloc(&(*(uint32_t **)&(array))[-2], sizeof(*(array)) * KeroDynamicArray_Size(array) + sizeof(uint32_t) * 2) + 2), \
(array) \
)
    
#if __cplusplus__
}
#endif

#define KERO_STD_H
#endif