#ifndef KERO_SOFTWARE_3D_H

#ifdef __cplusplus
extern "C"{
#endif
    
#include "kero_platform.h"
#include "kero_vec2.h"
#include "kero_vec3.h"
#include "kero_image.h"
#include "kero_sprite.h"
#include "kero_std.h"
#include <assert.h>
#include <limits.h>
    
    typedef struct {
        union {
            struct { vec3_t v0, v1, v2; };
            vec3_t v[3];
        };
        uint32_t c;
        struct {
            uint32_t double_sided: 1;
        } flags;
        vec2_t uv[3];
        union { uint8_t t, texture_index; };
    } face_t;
    
    typedef struct {
        union { uint32_t *pixels, *p; };
        union { float *depth, *d; };
        union { int width, w; };
        union { int height, h; };
    } k3d_frame_buffer_t;
    
    static inline void K3D_SetPixel(k3d_frame_buffer_t *target, int x, int y, float depth, uint32_t color){
        if(depth > target->depth[x + y*target->w]){
            target->pixels[y * target->w + x] = color;
            target->depth[x + y*target->w] = depth;
        }
    }
    
    static inline void K3D_SetPixelAlpha10(k3d_frame_buffer_t *target, int x, int y, float depth, uint32_t color){
        if((color>>24) > 0 && depth > target->depth[x + y*target->w]){
            target->pixels[y * target->w + x] = color;
            target->depth[x + y*target->w] = depth;
        }
    }
    
    static inline void K3D_ScanLine(k3d_frame_buffer_t *target, int y, int x0, float z0, int x1, float z1, uint32_t pixel){
        if(x0 > x1) {
            KS_Swap(int, x0, x1);
            KS_Swap(float, z0, z1);
        }
        if(x1 < 0 || x0 > target->w-1)return;
        int left = Max(0, x0);
        int right = Min(target->w-1, x1);
        for(int x = left; x <= right; ++x){
            float z = z0 + (z1-z0) * (((float)x-x0) / (x1-x0+0.0001f));
            K3D_SetPixel(target, x, y, z, pixel);
        }
    }
    
    static inline void K3D_ScanLineTextured(k3d_frame_buffer_t *target, ksprite_t* texture, int y, int x0, float z0, int x1, float z1, float u0, float u1, float v0, float v1){
        if(y < 0 || y > target->h-1) return;
        if(x0 > x1) {
            KS_Swap(int, x0, x1);
            if(x1 < 0 || x0 > target->w-1) return;
            KS_Swap(float, z0, z1);
            KS_Swap(float, u0, u1);
            KS_Swap(float, v0, v1);
        }
        u0 = KS_Min(1, KS_Max(0, u0));
        u1 = KS_Min(1, KS_Max(0, u1));
        v0 = KS_Min(1, KS_Max(0, v0));
        v1 = KS_Min(1, KS_Max(0, v1));
        int left = Max(0, x0);
        int right = Min(target->w-1, x1);
        uint32_t pixel;
        float skip = left - x0;
        float xfrac = 1.f/(x1-x0+0.0001f);
        float zstep = (z1-z0)*xfrac;
        float ustep = (u1-u0)*xfrac;
        float vstep = (v1-v0)*xfrac;
        float z = z0 + zstep*skip;
        float u = u0 + ustep*skip;
        float v = v0 + vstep*skip;
        for(int x = left; x <= right; ++x){
            K3D_SetPixelAlpha10(target, x, y, z, KS_SampleClamped(texture, u/z, v/z));
            z += zstep;
            u += ustep;
            v += vstep;
        }
    }
    
    static inline void K3D_ScanLineSafe(k3d_frame_buffer_t *target, int y, int x0, float z0, int x1, float z1, uint32_t pixel){
        if(y < 0 || y > target->h-1)return;
        int left = Min(x0, x1);
        int right = Max(x0, x1);
        if(right < 0 || left > target->w-1)return;
        left = Max(0, left);
        right = Min(target->w-1, right);
        for(int x = left; x <= right; ++x){
            K3D_SetPixel(target, x, y, z0, pixel);
        }
    }
    
    void K3D_DrawTriangle(k3d_frame_buffer_t *target, vec3_t a, vec3_t b, vec3_t c, uint32_t color){
        // Sort vertices vertically so a.y <= b.y <= c.y
        if(a.y > b.y){
            KS_Swap(vec3_t, a, b);
        }
        if(b.y > c.y){
            KS_Swap(vec3_t, b, c);
        }
        if(a.y > b.y){
            KS_Swap(vec3_t, a, b);
        }
        if(b.y>a.y){
            // Scan lines between the edge of a->b and the edge of a->c
            for(int y = KS_Max(a.y, 0); y <= KS_Min(b.y, target->h-1); ++y){
                float fracb = ((KS_Max((float)y,a.y)-a.y)/(b.y-a.y));
                float fracc = ((KS_Max((float)y,a.y)-a.y)/(c.y-a.y));
                float x01 = a.x+(b.x-a.x)*fracb;
                float x02 = a.x+(c.x-a.x)*fracc;
                float z0 =  a.z+(b.z-a.z)*fracb;
                float z1 =  a.z+(c.z-a.z)*fracc;
                K3D_ScanLine(target, y, x01, z0, x02, z1, color);
            }
        }
        if(c.y>b.y){
            // Scan lines between the edge of a->c and the edge of b->c
            for(int y = KS_Max(b.y, 0); y <= KS_Min(c.y, target->h-1); ++y){
                float fraca = ((KS_Max((float)y,b.y)-a.y)/(c.y-a.y));
                float fracb = ((KS_Max((float)y,b.y)-b.y)/(c.y-b.y));
                float x02 = a.x+(c.x-a.x)*fraca;
                float x12 = b.x+(c.x-b.x)*fracb;
                float z0 =  a.z+(c.z-a.z)*fraca;
                float z1 =  b.z+(c.z-b.z)*fracb;
                K3D_ScanLine(target, y, x02, z0, x12, z1, color);
            }
        }
    }
    
    void K3D_DrawTriangleTextured(k3d_frame_buffer_t *target, ksprite_t* texture, vec3_t a, vec3_t b, vec3_t c, vec2_t uv[3]){
        // Sort vertices vertically so a.y <= b.y <= c.y
        if(a.y > b.y){
            KS_Swap(vec3_t, a, b);
            KS_Swap(float, uv[0].u, uv[1].u);
            KS_Swap(float, uv[0].v, uv[1].v);
        }
        if(b.y > c.y){
            KS_Swap(vec3_t, b, c);
            KS_Swap(float, uv[1].u, uv[2].u);
            KS_Swap(float, uv[1].v, uv[2].v);
        }
        if(a.y > b.y){
            KS_Swap(vec3_t, a, b);
            KS_Swap(float, uv[0].u, uv[1].u);
            KS_Swap(float, uv[0].v, uv[1].v);
        }
        if(b.y>a.y){
            // Scan lines between the edge of a->b and the edge of a->c
            int y2 = KS_Min(b.y, target->h-1);
            float x0, x1, z0, z1, u0, u1, v0, v1;
            float fracab, fracac;
            for(int y = KS_Max(a.y, 0); y <= y2; ++y) {
                fracab = (KS_Max(y, a.y)-a.y)/(b.y-a.y), fracac = (KS_Max(y, a.y)-a.y)/(c.y-a.y);
                x0 = a.x+(b.x-a.x)*fracab, x1 = a.x+(c.x-a.x)*fracac;
                z0 = a.z+(b.z-a.z)*fracab, z1 = a.z+(c.z-a.z)*fracac;
                u0 = uv[0].u+(uv[1].u-uv[0].u)*fracab, u1 = uv[0].u+(uv[2].u-uv[0].u)*fracac;
                v0 = uv[0].v+(uv[1].v-uv[0].v)*fracab, v1 = uv[0].v+(uv[2].v-uv[0].v)*fracac;
                K3D_ScanLineTextured(target, texture, y, x0, z0, x1, z1, u0, u1, v0, v1);
            }
        }
        if(c.y>b.y){
            // Scan lines between the edge of a->c and the edge of b->c
            int y2 = KS_Min(c.y, target->h);
            float fracac, fracbc;
            float x0, x1, z0, z1, u0, u1, v0, v1;
            for(int y = KS_Max(b.y, 0); y < y2; ++y){
                fracac = (KS_Max(y, b.y)-a.y)/(c.y-a.y), fracbc = (KS_Max(y, b.y)-b.y)/(c.y-b.y);
                x0 = a.x+(c.x-a.x)*fracac, x1 = b.x+(c.x-b.x)*fracbc;
                z0 = a.z+(c.z-a.z)*fracac, z1 = b.z+(c.z-b.z)*fracbc;
                u0 = uv[0].u+(uv[2].u-uv[0].u)*fracac, u1 = uv[1].u+(uv[2].u-uv[1].u)*fracbc;
                v0 = uv[0].v+(uv[2].v-uv[0].v)*fracac, v1 = uv[1].v+(uv[2].v-uv[1].v)*fracbc;
                K3D_ScanLineTextured(target, texture, y, x0, z0, x1, z1, u0, u1, v0, v1);
            }
        }
    }
    
    static inline void K3D_DrawTriangleWire(ksprite_t *target, vec3_t a, vec3_t b, vec3_t c, uint32_t color){
        KS_DrawLine(target, a.x, a.y, b.x, b.y, color);
        KS_DrawLine(target, a.x, a.y, c.x, c.y, color);
        KS_DrawLine(target, b.x, b.y, c.x, c.y, color);
    }
    
    static inline void K3D_RenderQuad(vec3_t a, vec3_t b, vec3_t c, vec3_t d, uint32_t color, k3d_frame_buffer_t *target){
        K3D_DrawTriangle(target, a, b, c, color);
        K3D_DrawTriangle(target, a, c, d, color);
    }
    
    void K3D_CreateDodecahedron(face_t faces[36], float r) {
        float s = 1.f;
        float t2 = PI / 10.f;
        float t3 = 3.f * PI / 10.f;
        float t4 = PI / 5.f;
        float d1 = s / 2.f / sinf(t4);
        float d2 = d1 * cosf(t4);
        float d3 = d1 * cosf(t2);
        float d4 = d1 * sinf(t2);
        float Fx =
            (s * s - (2.f * d3) * (2.f * d3) -
             (d1 * d1 - d3 * d3 - d4 * d4)) /
            (2.f * (d4 - d1));
        float d5 = SquareRoot(0.5f *
                              (s * s + (2.f * d3) * (2.f * d3) -
                               (d1 - Fx) * (d1 - Fx) -
                               (d4 - Fx) * (d4 - Fx) - d3 * d3));
        float Fy = (Fx * Fx - d1 * d1 - d5 * d5) / (2.f * d5);
        float Ay = d5 + Fy;
        vec3_t verts[20] = {
            Vec3MulScalar(Vec3Make(d1, Ay, 0), r),
            Vec3MulScalar(Vec3Make(d4, Ay, d3), r),
            Vec3MulScalar(Vec3Make(-d2, Ay, s / 2), r),
            Vec3MulScalar(Vec3Make(-d2, Ay, -s / 2), r),
            Vec3MulScalar(Vec3Make(d4, Ay, -d3), r),
            Vec3MulScalar(Vec3Make(Fx, Fy, 0), r),
            Vec3MulScalar(Vec3Make(Fx * sinf(t2), Fy, Fx * cosf(t2)), r),
            Vec3MulScalar(Vec3Make(-Fx * sinf(t3), Fy, Fx * cosf(t3)), r),
            Vec3MulScalar(Vec3Make(-Fx * sinf(t3), Fy, -Fx * cosf(t3)), r),
            Vec3MulScalar(Vec3Make(Fx * sinf(t2), Fy, -Fx * cosf(t2)), r),
            Vec3MulScalar(Vec3Make(Fx * sinf(t3), -Fy, Fx * cosf(t3)), r),
            Vec3MulScalar(Vec3Make(-Fx * sinf(t2), -Fy, Fx * cosf(t2)), r),
            Vec3MulScalar(Vec3Make(-Fx, -Fy, 0), r),
            Vec3MulScalar(Vec3Make(-Fx * sinf(t2), -Fy, -Fx * cosf(t2)), r),
            Vec3MulScalar(Vec3Make(Fx * sinf(t3), -Fy, -Fx * cosf(t3)), r),
            Vec3MulScalar(Vec3Make(d2, -Ay, s / 2), r),
            Vec3MulScalar(Vec3Make(-d4, -Ay, d3), r),
            Vec3MulScalar(Vec3Make(-d1, -Ay, 0), r),
            Vec3MulScalar(Vec3Make(-d4, -Ay, -d3), r),
            Vec3MulScalar(Vec3Make(d2, -Ay, -s / 2), r),
        };
        for(int i = 0; i < 12; ++i) {
            faces[i*3].c = (rand()%(255));
            faces[i*3].c = (faces[i*3].c<<16) + (faces[i*3].c<<8) + (faces[i*3].c) + (255<<24);
            faces[i*3].flags.double_sided = false;
            faces[i*3].texture_index = -1;
            for(int j = i*3+1; j < i*3+3; ++j) {
                faces[j].c = faces[i*3].c;
                faces[j].flags = faces[i].flags;
                faces[j].texture_index = faces[i].texture_index;
            }
        }
        faces[ 0].v0 = verts[ 0], faces[ 0].v1 = verts[ 2], faces[ 0].v2 = verts[ 1];
        faces[ 1].v0 = verts[ 0], faces[ 1].v1 = verts[ 3], faces[ 1].v2 = verts[ 2];
        faces[ 2].v0 = verts[ 0], faces[ 2].v1 = verts[ 4], faces[ 2].v2 = verts[ 3];
        faces[ 3].v0 = verts[ 0], faces[ 3].v1 = verts[ 1], faces[ 3].v2 = verts[ 5];
        faces[ 4].v0 = verts[ 1], faces[ 4].v1 = verts[ 6], faces[ 4].v2 = verts[ 5];
        faces[ 5].v0 = verts[ 5], faces[ 5].v1 = verts[ 6], faces[ 5].v2 = verts[10];
        faces[ 6].v0 = verts[ 0], faces[ 6].v1 = verts[ 5], faces[ 6].v2 = verts[ 9];
        faces[ 7].v0 = verts[ 0], faces[ 7].v1 = verts[ 9], faces[ 7].v2 = verts[ 4];
        faces[ 8].v0 = verts[ 9], faces[ 8].v1 = verts[ 5], faces[ 8].v2 = verts[14];
        faces[ 9].v0 = verts[ 1], faces[ 9].v1 = verts[ 2], faces[ 9].v2 = verts[ 7];
        faces[10].v0 = verts[ 1], faces[10].v1 = verts[ 7], faces[10].v2 = verts[ 6];
        faces[11].v0 = verts[ 7], faces[11].v1 = verts[11], faces[11].v2 = verts[ 6];
        faces[12].v0 = verts[ 2], faces[12].v1 = verts[ 3], faces[12].v2 = verts[ 8];
        faces[13].v0 = verts[ 2], faces[13].v1 = verts[ 8], faces[13].v2 = verts[ 7];
        faces[14].v0 = verts[ 8], faces[14].v1 = verts[12], faces[14].v2 = verts[ 7];
        faces[15].v0 = verts[ 3], faces[15].v1 = verts[ 4], faces[15].v2 = verts[ 9];
        faces[16].v0 = verts[ 3], faces[16].v1 = verts[ 9], faces[16].v2 = verts[ 8];
        faces[17].v0 = verts[ 9], faces[17].v1 = verts[13], faces[17].v2 = verts[ 8];
        faces[18].v0 = verts[ 5], faces[18].v1 = verts[10], faces[18].v2 = verts[15];
        faces[19].v0 = verts[ 5], faces[19].v1 = verts[15], faces[19].v2 = verts[19];
        faces[20].v0 = verts[ 5], faces[20].v1 = verts[19], faces[20].v2 = verts[14];
        faces[21].v0 = verts[ 9], faces[21].v1 = verts[14], faces[21].v2 = verts[19];
        faces[22].v0 = verts[ 9], faces[22].v1 = verts[19], faces[22].v2 = verts[18];
        faces[23].v0 = verts[ 9], faces[23].v1 = verts[18], faces[23].v2 = verts[13];
        faces[24].v0 = verts[ 8], faces[24].v1 = verts[13], faces[24].v2 = verts[18];
        faces[25].v0 = verts[ 8], faces[25].v1 = verts[18], faces[25].v2 = verts[17];
        faces[26].v0 = verts[ 8], faces[26].v1 = verts[17], faces[26].v2 = verts[12];
        faces[27].v0 = verts[ 7], faces[27].v1 = verts[12], faces[27].v2 = verts[17];
        faces[28].v0 = verts[ 7], faces[28].v1 = verts[17], faces[28].v2 = verts[16];
        faces[29].v0 = verts[ 7], faces[29].v1 = verts[16], faces[29].v2 = verts[11];
        faces[30].v0 = verts[ 6], faces[30].v1 = verts[11], faces[30].v2 = verts[16];
        faces[31].v0 = verts[ 6], faces[31].v1 = verts[16], faces[31].v2 = verts[15];
        faces[32].v0 = verts[ 6], faces[32].v1 = verts[15], faces[32].v2 = verts[10];
        faces[33].v0 = verts[15], faces[33].v1 = verts[16], faces[33].v2 = verts[17];
        faces[34].v0 = verts[15], faces[34].v1 = verts[17], faces[34].v2 = verts[18];
        faces[35].v0 = verts[15], faces[35].v1 = verts[18], faces[35].v2 = verts[19];
    }
    
    face_t K3D_TranslateRotate(face_t before, vec3_t translation, vec3_t rotation) {
        face_t temp1, temp2;
        for(int v = 0; v < 3; ++v) {
            // Roll
            temp2.v[v].x = cos(rotation.z)*before.v[v].x - sin(rotation.z)*before.v[v].y;
            temp2.v[v].z = before.v[v].z;
            temp2.v[v].y = sin(rotation.z)*before.v[v].x + cos(rotation.z)*before.v[v].y;
            // Pitch
            temp1.v[v].x = temp2.v[v].x;
            temp1.v[v].y = cos(rotation.x)*temp2.v[v].y - sin(rotation.x)*temp2.v[v].z;
            temp1.v[v].z = sin(rotation.x)*temp2.v[v].y + cos(rotation.x)*temp2.v[v].z;
            // Yaw
            temp2.v[v].x = cos(rotation.y)*temp1.v[v].x - sin(rotation.y)*temp1.v[v].z;
            temp2.v[v].y = temp1.v[v].y;
            temp2.v[v].z = sin(rotation.y)*temp1.v[v].x + cos(rotation.y)*temp1.v[v].z;
            // Translate 
            temp1.v[v] = Vec3Add(temp2.v[v], translation);
        }
        temp1.texture_index = before.texture_index;
        temp1.c = before.c;
        temp1.flags = before.flags;
        temp1.uv[0].u = before.uv[0].u;
        temp1.uv[1].u = before.uv[1].u;
        temp1.uv[2].u = before.uv[2].u;
        temp1.uv[0].v = before.uv[0].v;
        temp1.uv[1].v = before.uv[1].v;
        temp1.uv[2].v = before.uv[2].v;
        return temp1;
    }
    
    face_t K3D_CameraTranslateRotate(face_t before, vec3_t translation, vec3_t rotation) {
        face_t temp1, temp2;
        for(int v = 0; v < 3; ++v) {
            // Translate 
            temp1.v[v] = Vec3Sub(before.v[v], translation);
            // Yaw
            temp2.v[v].x = sin(-rotation.y)*temp1.v[v].x - cos(-rotation.y)*temp1.v[v].z;
            temp2.v[v].y = temp1.v[v].y;
            temp2.v[v].z = cos(-rotation.y)*temp1.v[v].x + sin(-rotation.y)*temp1.v[v].z;
            // Pitch
            temp1.v[v].x = temp2.v[v].x;
            temp1.v[v].y = sin(rotation.x)*temp2.v[v].y - cos(rotation.x)*temp2.v[v].z;
            temp1.v[v].z = cos(rotation.x)*temp2.v[v].y + sin(rotation.x)*temp2.v[v].z;
            // Roll
            temp2.v[v].x = cos(rotation.z)*temp1.v[v].x - sin(rotation.z)*temp1.v[v].y;
            temp2.v[v].y = sin(rotation.z)*temp1.v[v].x + cos(rotation.z)*temp1.v[v].y;
            temp2.v[v].z = temp1.v[v].z;
        }
        temp2.texture_index = before.texture_index;
        temp2.c = before.c;
        temp2.flags = before.flags;
        temp2.uv[0].u = before.uv[0].u;
        temp2.uv[1].u = before.uv[1].u;
        temp2.uv[2].u = before.uv[2].u;
        temp2.uv[0].v = before.uv[0].v;
        temp2.uv[1].v = before.uv[1].v;
        temp2.uv[2].v = before.uv[2].v;
        return temp2;
    }
    
    typedef struct {
        union { vec3_t position, pos, p; };
        union { vec3_t rotation, rot, r; };
        float near, far;
        union { face_t *faces, *f; };
        union { int face_count, fc; };
        union { int next_face, nf; };
    } k3d_camera_euler_t;
    
    void K3D_FPCameraClampRotation(k3d_camera_euler_t *c) {
        if(Absolute(c->r.yaw) > TWOPI) {
            c->r.yaw -= Sign(c->r.yaw) * TWOPI;
        }
        if(c->r.pitch < 0.f) {
            c->r.pitch = 0.f;
        }
        else if(c->r.pitch > PI) {
            c->r.pitch = PI;
        }
        if(Absolute(c->r.roll) > TWOPI) {
            c->r.roll -= Sign(c->r.roll) * TWOPI;
        }
    }
    
    typedef struct {
        union { vec3_t position, pos, p; };
        union { vec3_t rotation, rot, r; };
        union { unsigned int face_count, fc; };
        union { face_t *faces, *f; };
    } k3d_object_t;
    
    typedef struct {
        union { face_t *faces, *f; };
        union { int face_count, fc; };
        union { int next_face, nf; };
    } k3d_world_t;
    
    void K3D_ClearFrameBuffer(k3d_frame_buffer_t *frame_buffer) {
        memset(frame_buffer->p, 0, frame_buffer->w*frame_buffer->h*sizeof(uint32_t));
        memset(frame_buffer->d, 0, frame_buffer->w*frame_buffer->h*sizeof(float));
    }
    
    void K3D_ObjectToWorld(k3d_object_t *object, k3d_world_t *world) {
        for(int f = 0; f < object->fc; ++f) {
            world->f[world->nf] = K3D_TranslateRotate(object->faces[f], object->pos, object->rot);
            ++world->nf;
        }
    }
    
    void K3D_ResetForFrame(k3d_frame_buffer_t *frame_buffer, k3d_world_t *world) {
        K3D_ClearFrameBuffer(frame_buffer);
        world->nf = 0;
    }
    
    void K3D_WorldToCameraEuler(k3d_world_t *world, k3d_camera_euler_t *camera) {
        int cam_it = 0,  world_it = 0;
        face_t world_face_trans, world_face_rot, world_face_clipped[2], temp;
        vec3_t v[4];
        int num_verts_to_clip, verts_to_clip;
        for(; world_it < world->nf; ++world_it) {
            if(!world->f[world_it].flags.double_sided) {
                // back-face culling
                vec3_t n = Vec3Norm(Vec3Cross(Vec3AToB(world->f[world_it].v0, world->f[world_it].v1), Vec3AToB(world->f[world_it].v0, world->f[world_it].v2)));
                vec3_t poly_to_cam = Vec3AToB(world->f[world_it].v0, camera->pos);
                float angle = Vec3Dot(n, poly_to_cam);
                if(angle <= 0) continue;
            }
            world_face_rot = K3D_CameraTranslateRotate(world->f[world_it], camera->pos, camera->rot);
            
            if(world_face_rot.v0.z < camera->near && world_face_rot.v1.z < camera->near && world_face_rot.v2.z < camera->near/* || world_face_rot.v0.z > camera->far && world_face_rot.v1.z > camera->far && world_face_rot.v2.z > camera->far*/) continue;
            // clip on camera->near plane
            num_verts_to_clip = verts_to_clip = 0;
            if(world_face_rot.v0.z < camera->near) {
                ++num_verts_to_clip;
                verts_to_clip |= 0b1;
            }
            if(world_face_rot.v1.z < camera->near) {
                ++num_verts_to_clip;
                verts_to_clip |= 0b10;
            }
            if(world_face_rot.v2.z < camera->near) {
                ++num_verts_to_clip;
                verts_to_clip |= 0b100;
            }
            vec2_t uv[4] = {
                {{{ world_face_rot.uv[0].u, world_face_rot.uv[0].v }}},
                {{{ world_face_rot.uv[1].u, world_face_rot.uv[1].v }}},
                {{{ world_face_rot.uv[2].u, world_face_rot.uv[2].v }}},
            };
            
            if(num_verts_to_clip == 0) {
                // All verts are >= camera->near. No clipping
                camera->faces[cam_it++] = world_face_rot;
            }
            else if(num_verts_to_clip == 1) {
                if(verts_to_clip == 0b1) { // Clip v0
                    // Distance along v0->v1 where z == camera->near
                    float t = (camera->near-world_face_rot.v0.z)/(world_face_rot.v1.z-world_face_rot.v0.z); 
                    v[0].x = world_face_rot.v0.x + (world_face_rot.v1.x - world_face_rot.v0.x) * t;
                    v[0].y = world_face_rot.v0.y + (world_face_rot.v1.y - world_face_rot.v0.y) * t;
                    v[0].z = camera->near;
                    uv[0].u = world_face_rot.uv[0].u+(world_face_rot.uv[1].u-world_face_rot.uv[0].u)*t;
                    uv[0].v = world_face_rot.uv[0].v+(world_face_rot.uv[1].v-world_face_rot.uv[0].v)*t;
                    // Distance along v0->v2 where z == camera->near
                    t = (camera->near-world_face_rot.v0.z)/(world_face_rot.v2.z-world_face_rot.v0.z);
                    v[3].x = world_face_rot.v0.x + (world_face_rot.v2.x - world_face_rot.v0.x) * t;
                    v[3].y = world_face_rot.v0.y + (world_face_rot.v2.y - world_face_rot.v0.y) * t;
                    v[3].z = camera->near;
                    uv[3].u = world_face_rot.uv[0].u+(world_face_rot.uv[2].u-world_face_rot.uv[0].u)*t;
                    uv[3].v = world_face_rot.uv[0].v+(world_face_rot.uv[2].v-world_face_rot.uv[0].v)*t;
                    v[1] = world_face_rot.v1;
                    v[2] = world_face_rot.v2;
                }
                
                else if(verts_to_clip == 0b10) { // Clip v1
                    // Distance along v1->v2 where z == camera->near
                    float t = (camera->near-world_face_rot.v1.z)/(world_face_rot.v2.z-world_face_rot.v1.z); 
                    v[0].x = world_face_rot.v1.x + (world_face_rot.v2.x - world_face_rot.v1.x) * t;
                    v[0].y = world_face_rot.v1.y + (world_face_rot.v2.y - world_face_rot.v1.y) * t;
                    v[0].z = camera->near;
                    uv[0].u = world_face_rot.uv[1].u+(world_face_rot.uv[2].u-world_face_rot.uv[1].u)*t;
                    uv[0].v = world_face_rot.uv[1].v+(world_face_rot.uv[2].v-world_face_rot.uv[1].v)*t;
                    // Distance along v1->v0 where z == camera->near
                    t = (camera->near-world_face_rot.v1.z)/(world_face_rot.v0.z-world_face_rot.v1.z); 
                    v[3].x = world_face_rot.v1.x + (world_face_rot.v0.x - world_face_rot.v1.x) * t;
                    v[3].y = world_face_rot.v1.y + (world_face_rot.v0.y - world_face_rot.v1.y) * t;
                    v[3].z = camera->near;
                    uv[3].u = world_face_rot.uv[1].u+(world_face_rot.uv[0].u-world_face_rot.uv[1].u)*t;
                    uv[3].v = world_face_rot.uv[1].v+(world_face_rot.uv[0].v-world_face_rot.uv[1].v)*t;
                    v[1] = world_face_rot.v2;
                    v[2] = world_face_rot.v0;
                    uv[1].u = world_face_rot.uv[2].u;
                    uv[2].u = world_face_rot.uv[0].u;
                    uv[1].v = world_face_rot.uv[2].v;
                    uv[2].v = world_face_rot.uv[0].v;
                }
                
                else if(verts_to_clip == 0b100) { // Clip v2
                    // Distance along v2->v0 where z == camera->near
                    float t = (camera->near-world_face_rot.v2.z)/(world_face_rot.v0.z-world_face_rot.v2.z);
                    v[0].x = world_face_rot.v2.x + (world_face_rot.v0.x - world_face_rot.v2.x) * t;
                    v[0].y = world_face_rot.v2.y + (world_face_rot.v0.y - world_face_rot.v2.y) * t;
                    v[0].z = camera->near;
                    uv[0].u = world_face_rot.uv[2].u+(world_face_rot.uv[0].u-world_face_rot.uv[2].u)*t;
                    uv[0].v = world_face_rot.uv[2].v+(world_face_rot.uv[0].v-world_face_rot.uv[2].v)*t;
                    // Distance along v2->v1 where z == camera->near
                    t = (camera->near-world_face_rot.v2.z)/(world_face_rot.v1.z-world_face_rot.v2.z); 
                    v[3].x = world_face_rot.v2.x + (world_face_rot.v1.x - world_face_rot.v2.x) * t;
                    v[3].y = world_face_rot.v2.y + (world_face_rot.v1.y - world_face_rot.v2.y) * t;
                    v[3].z = camera->near;
                    uv[3].u = world_face_rot.uv[2].u+(world_face_rot.uv[1].u-world_face_rot.uv[2].u)*t;
                    uv[3].v = world_face_rot.uv[2].v+(world_face_rot.uv[1].v-world_face_rot.uv[2].v)*t;
                    v[1] = world_face_rot.v0;
                    v[2] = world_face_rot.v1;
                    uv[1].u = world_face_rot.uv[0].u;
                    uv[2].u = world_face_rot.uv[1].u;
                    uv[1].v = world_face_rot.uv[0].v;
                    uv[2].v = world_face_rot.uv[1].v;
                }
                
                camera->faces[cam_it].c = camera->faces[cam_it+1].c = world_face_rot.c;
                camera->faces[cam_it].v0 = v[0];
                camera->faces[cam_it].v1 = v[1];
                camera->faces[cam_it].v2 = v[2];
                camera->faces[cam_it].texture_index = world_face_rot.texture_index;
                camera->faces[cam_it].c = world_face_rot.c;
                camera->faces[cam_it].flags = world_face_rot.flags;
                camera->faces[cam_it].uv[0].u = uv[0].u;
                camera->faces[cam_it].uv[1].u = uv[1].u;
                camera->faces[cam_it].uv[2].u = uv[2].u;
                camera->faces[cam_it].uv[0].v = uv[0].v;
                camera->faces[cam_it].uv[1].v = uv[1].v;
                camera->faces[cam_it].uv[2].v = uv[2].v;
                ++cam_it;
                camera->faces[cam_it].v0 = v[0];
                camera->faces[cam_it].v1 = v[2];
                camera->faces[cam_it].v2 = v[3];
                camera->faces[cam_it].texture_index = world_face_rot.texture_index;
                camera->faces[cam_it].c = world_face_rot.c;
                camera->faces[cam_it].flags = world_face_rot.flags;
                camera->faces[cam_it].uv[0].u = uv[0].u;
                camera->faces[cam_it].uv[1].u = uv[2].u;
                camera->faces[cam_it].uv[2].u = uv[3].u;
                camera->faces[cam_it].uv[0].v = uv[0].v;
                camera->faces[cam_it].uv[1].v = uv[2].v;
                camera->faces[cam_it].uv[2].v = uv[3].v;
                ++cam_it;
            }
            
            else if(num_verts_to_clip == 2) {
                if(verts_to_clip & 0b1) { // Clip v0
                    if(verts_to_clip & 0b10) { // v1 will be clipped so use v2
                        float t = (camera->near-world_face_rot.v0.z)/(world_face_rot.v2.z-world_face_rot.v0.z); // Distance along v0->v2 where z == camera->near
                        v[0].x = world_face_rot.v0.x + (world_face_rot.v2.x - world_face_rot.v0.x) * t;
                        v[0].y = world_face_rot.v0.y + (world_face_rot.v2.y - world_face_rot.v0.y) * t;
                        v[0].z = camera->near;
                        uv[0].u = world_face_rot.uv[0].u+(world_face_rot.uv[2].u-world_face_rot.uv[0].u)*t;
                        uv[0].v = world_face_rot.uv[0].v+(world_face_rot.uv[2].v-world_face_rot.uv[0].v)*t;
                    }
                    
                    else { // v2 will be clipped so use v1
                        float t = (camera->near-world_face_rot.v0.z)/(world_face_rot.v1.z-world_face_rot.v0.z); // Distance along v0->v1 where z == camera->near
                        v[0].x = world_face_rot.v0.x + (world_face_rot.v1.x - world_face_rot.v0.x) * t;
                        v[0].y = world_face_rot.v0.y + (world_face_rot.v1.y - world_face_rot.v0.y) * t;
                        v[0].z = camera->near;
                        uv[0].u = world_face_rot.uv[0].u+(world_face_rot.uv[1].u-world_face_rot.uv[0].u)*t;
                        uv[0].v = world_face_rot.uv[0].v+(world_face_rot.uv[1].v-world_face_rot.uv[0].v)*t;
                    }
                }
                else {
                    v[0] = world_face_rot.v0;
                }
                
                if(verts_to_clip & 0b10) { // Clip v1
                    if(verts_to_clip & 0b1) { // v0 will be clipped so use v2
                        float t = (camera->near-world_face_rot.v1.z)/(world_face_rot.v2.z-world_face_rot.v1.z); // Distance along v1->v2 where z == camera->near
                        v[1].x = world_face_rot.v1.x + (world_face_rot.v2.x - world_face_rot.v1.x) * t;
                        v[1].y = world_face_rot.v1.y + (world_face_rot.v2.y - world_face_rot.v1.y) * t;
                        v[1].z = camera->near;
                        uv[1].u = world_face_rot.uv[1].u+(world_face_rot.uv[2].u-world_face_rot.uv[1].u)*t;
                        uv[1].v = world_face_rot.uv[1].v+(world_face_rot.uv[2].v-world_face_rot.uv[1].v)*t;
                    }
                    
                    else { // v2 will be clipped so use v0
                        float t = (camera->near-world_face_rot.v1.z)/(world_face_rot.v0.z-world_face_rot.v1.z); // Distance along v1->v0 where z == camera->near
                        v[1].x = world_face_rot.v1.x + (world_face_rot.v0.x - world_face_rot.v1.x) * t;
                        v[1].y = world_face_rot.v1.y + (world_face_rot.v0.y - world_face_rot.v1.y) * t;
                        v[1].z = camera->near;
                        uv[1].u = world_face_rot.uv[1].u+(world_face_rot.uv[0].u-world_face_rot.uv[1].u)*t;
                        uv[1].v = world_face_rot.uv[1].v+(world_face_rot.uv[0].v-world_face_rot.uv[1].v)*t;
                    }
                }
                else {
                    v[1] = world_face_rot.v1;
                }
                
                if(verts_to_clip & 0b100) { // Clip v2
                    if(verts_to_clip & 0b1) { // v0 will be clipped so use v1
                        float t = (camera->near-world_face_rot.v2.z)/(world_face_rot.v1.z-world_face_rot.v2.z); // Distance along v2->v1 where z == camera->near
                        v[2].x = world_face_rot.v2.x + (world_face_rot.v1.x - world_face_rot.v2.x) * t;
                        v[2].y = world_face_rot.v2.y + (world_face_rot.v1.y - world_face_rot.v2.y) * t;
                        v[2].z = camera->near;
                        uv[2].u = world_face_rot.uv[2].u+(world_face_rot.uv[1].u-world_face_rot.uv[2].u)*t;
                        uv[2].v = world_face_rot.uv[2].v+(world_face_rot.uv[1].v-world_face_rot.uv[2].v)*t;
                    }
                    
                    else { // v2 will be clipped so use v0
                        float t = (camera->near-world_face_rot.v2.z)/(world_face_rot.v0.z-world_face_rot.v2.z); // Distance along v1->v0 where z == camera->near
                        v[2].x = world_face_rot.v2.x + (world_face_rot.v0.x - world_face_rot.v2.x) * t;
                        v[2].y = world_face_rot.v2.y + (world_face_rot.v0.y - world_face_rot.v2.y) * t;
                        v[2].z = camera->near;
                        uv[2].u = world_face_rot.uv[2].u+(world_face_rot.uv[0].u-world_face_rot.uv[2].u)*t;
                        uv[2].v = world_face_rot.uv[2].v+(world_face_rot.uv[0].v-world_face_rot.uv[2].v)*t;
                    }
                }
                else {
                    v[2] = world_face_rot.v2;
                }
                
                camera->faces[cam_it].v0 = v[0];
                camera->faces[cam_it].v1 = v[1];
                camera->faces[cam_it].v2 = v[2];
                camera->faces[cam_it].c = world_face_rot.c;
                camera->faces[cam_it].flags = world_face_rot.flags;
                camera->faces[cam_it].texture_index = world_face_rot.texture_index;
                camera->faces[cam_it].uv[0].u = uv[0].u;
                camera->faces[cam_it].uv[1].u = uv[1].u;
                camera->faces[cam_it].uv[2].u = uv[2].u;
                camera->faces[cam_it].uv[0].v = uv[0].v;
                camera->faces[cam_it].uv[1].v = uv[1].v;
                camera->faces[cam_it].uv[2].v = uv[2].v;
                ++cam_it;
            }
            else { // Should have already clipped this before so fail assertion.
                assert(false);
            }
        }
        camera->next_face = cam_it;
    }
    
    typedef struct {
        union { int width, w; };
        union { int height, h; };
        union { float aspect, a; };
        union { face_t *faces, *f; };
        union { int face_count, fc; };
        union { int next_face, nf; };
    } k3d_view_t;
    
    void K3D_CameraEulerToView(k3d_camera_euler_t *camera, k3d_view_t *view) {
        int view_it = 0, cam_it = 0;
        for(; view_it < camera->nf; ++view_it, ++cam_it) {
            for(int v = 0; v < 3; ++v) {
                view->f[view_it].v[v].x = (camera->f[cam_it].v[v].x / camera->f[cam_it].v[v].z + 1) * view->width/2;
                view->f[view_it].v[v].y = (view->aspect * -camera->f[cam_it].v[v].y / camera->f[cam_it].v[v].z + 1) * view->height/2;
                view->f[view_it].v[v].z = camera->near / camera->f[cam_it].v[v].z;
            }
            view->f[view_it].c = camera->f[cam_it].c;
            view->f[view_it].flags = camera->f[cam_it].flags;
            view->f[view_it].texture_index = camera->f[cam_it].texture_index;
            view->f[view_it].uv[0].u = camera->f[cam_it].uv[0].u / camera->f[cam_it].v0.z;
            view->f[view_it].uv[1].u = camera->f[cam_it].uv[1].u / camera->f[cam_it].v1.z;
            view->f[view_it].uv[2].u = camera->f[cam_it].uv[2].u / camera->f[cam_it].v2.z;
            view->f[view_it].uv[0].v = camera->f[cam_it].uv[0].v / camera->f[cam_it].v0.z;
            view->f[view_it].uv[1].v = camera->f[cam_it].uv[1].v / camera->f[cam_it].v1.z;
            view->f[view_it].uv[2].v = camera->f[cam_it].uv[2].v / camera->f[cam_it].v2.z;
        }
        view->nf = view_it;
    }
    
#define K3D_TEXTURE_SIZE 64
#define K3D_TEXTURE_COUNT 64
#define K3D_TEXTURE_NAME_LENGTH 32
    typedef struct {
        bool used[K3D_TEXTURE_COUNT];
        union { ksprite_t t[K3D_TEXTURE_COUNT], tex[K3D_TEXTURE_COUNT], textures[K3D_TEXTURE_COUNT]; };
        uint32_t pixels[K3D_TEXTURE_COUNT][K3D_TEXTURE_SIZE * K3D_TEXTURE_SIZE];
        union { char n[K3D_TEXTURE_COUNT][K3D_TEXTURE_NAME_LENGTH+1], names[K3D_TEXTURE_COUNT][K3D_TEXTURE_NAME_LENGTH+1]; };
    } k3d_texture_set_t;
    
    int K3DTS_GetIndex(k3d_texture_set_t *textures, char *name) {
        if(strlen(name) > K3D_TEXTURE_NAME_LENGTH) {
            name[K3D_TEXTURE_NAME_LENGTH] = '\0';
        }
        for(int t = 0; t < K3D_TEXTURE_COUNT; ++t) {
            if(!textures->used[t]) continue;
            if(strcmp(name, textures->n[t]) == 0) {
                return t;
            }
        }
        return -1;
    }
    
    void K3D_TexturesInit(k3d_texture_set_t *textures) {
        for(int t = 0; t < K3D_TEXTURE_COUNT; ++t) {
            textures->used[t] = false;
            textures->t[t].pixels = textures->pixels[t];
            textures->t[t].w = textures->t[t].h = K3D_TEXTURE_SIZE;
            textures->n[t][0] = '\0';
        }
    }
    
    bool K3D_TexturesLoad(k3d_texture_set_t *textures, char *filepath) {
        int texture_index = 0;
        while(textures->used[texture_index]) ++texture_index;
        if(texture_index > K3D_TEXTURE_COUNT) {
            fprintf(stderr, "K3D_TexturesLoad(): Maximum textures already loaded.\n");
            return false;
        }
        
        kimage_t image;
        if(KI_Load(&image, filepath, 2, 1, 0, 3, 0) != KI_LOAD_SUCCESS) {
            fprintf(stderr, "Failed to load texture: %s\n", filepath);
            return false;
        }
        if(image.w != K3D_TEXTURE_SIZE || image.h != K3D_TEXTURE_SIZE) {
            fprintf(stderr, "K3D_TexturesLoad(): Image \"%s\" has incorrect dimensions. Is %dx%d. Should be %dx%d\n", filepath, image.w, image.h, K3D_TEXTURE_SIZE, K3D_TEXTURE_SIZE);
        }
        memcpy(textures->pixels[texture_index], image.pixels, sizeof(image.pixels[0]) * K3D_TEXTURE_SIZE * K3D_TEXTURE_SIZE);
        KI_Free(&image);
        
        int name_start, name_end;
        name_end = strlen(filepath) - 4;
        name_start = name_end;
        while(filepath[name_start-1] != '/') --name_start;
        strncpy(textures->n[texture_index], &filepath[name_start], name_end - name_start);
        textures->used[texture_index] = true;
        
        return true;
    }
    
    void K3D_ViewToScreen(k3d_view_t *view, k3d_frame_buffer_t *frame, k3d_texture_set_t *textures) {
        for(int i = 0; i < view->nf; ++i) {
            //K3D_DrawTriangleWire(frame, view->f[i].v0, view->f[i].v1, view->f[i].v2, view->f[i].c);
            // Textured
            if(view->f[i].texture_index < K3D_TEXTURE_COUNT) {
                K3D_DrawTriangleTextured(frame, &textures->t[view->f[i].texture_index], view->f[i].v0, view->f[i].v1, view->f[i].v2, view->f[i].uv);
            }
            else {
                //printf("%f %f %f %f %f %f %f %f %f %u\n", view->f[i].v0.x, view->f[i].v0.y, view->f[i].v0.z, view->f[i].v1.x, view->f[i].v1.y, view->f[i].v1.z, view->f[i].v2.x, view->f[i].v2.y, view->f[i].v2.z, view->f[i].c);
                K3D_DrawTriangle(frame, view->f[i].v0, view->f[i].v1, view->f[i].v2, view->f[i].c);
            }
            //KS_DrawTriangle(&frame_buffer, view->f[i].v0.x, view->f[i].v0.y, view->f[i].v1.x, view->f[i].v1.y, view->f[i].v2.x, view->f[i].v2.y, view->f[i].c);
            //K3D_DrawTrianglef(&frame_buffer, view->f[i].v0, view->f[i].v1, view->f[i].v2, view->f[i].c);
            /*KS_DrawLine(&frame_buffer, view->f[i].v0.x, view->f[i].v0.y, view->f[i].v1.x, view->f[i].v1.y, view->f[i].c, KSSetPixel);
            KS_DrawLine(&frame_buffer, view->f[i].v1.x, view->f[i].v1.y, view->f[i].v2.x, view->f[i].v2.y, view->f[i].c, KSSetPixel);
            KS_DrawLine(&frame_buffer, view->f[i].v2.x, view->f[i].v2.y, view->f[i].v0.x, view->f[i].v0.y, view->f[i].c, KSSetPixel);*/
        }
    }
    
    typedef struct {
        union {
            struct { int v0, v1, v2; };
            int v[3];
        };
        struct {
            uint32_t double_sided: 1;
        } flags;
        int uv[3];
        union { uint8_t t, texture_index; };
        int m;
    } k3d_obj_face_t;
    
    typedef struct {
        union { unsigned int face_count, fc; };
        union { face_t *faces, *f; };
    } k3d_model_textured_t;
    
    void K3D_TexturedQuad(k3d_model_textured_t *model, int texture_index, int width, int height) { // Be sure to K3D_ModelTexturedFree() when done
        model->faces = malloc(sizeof(face_t) * 2);
        model->fc = 2;
        model->faces[0].v0 = Vec3Make(-width, -height, 0);
        model->faces[0].v1 = Vec3Make(-width,  height, 0);
        model->faces[0].v2 = Vec3Make( width, -height, 0);
        model->faces[0].uv[0] = Vec2Make(0, 0);
        model->faces[0].uv[1] = Vec2Make(0, 1);
        model->faces[0].uv[2] = Vec2Make(1, 0);
        model->faces[0].c = 0xffffffff;
        model->faces[0].flags.double_sided = false;
        model->faces[0].t = texture_index;
        model->faces[1].v0 = Vec3Make( width, -height, 0);
        model->faces[1].v1 = Vec3Make(-width,  height, 0);
        model->faces[1].v2 = Vec3Make( width,  height, 0);
        model->faces[1].uv[0] = Vec2Make(1, 0);
        model->faces[1].uv[1] = Vec2Make(0, 1);
        model->faces[1].uv[2] = Vec2Make(1, 1);
        model->faces[1].c = 0xffffffff;
        model->faces[1].flags.double_sided = false;
        model->faces[1].t = texture_index;
    }
    
    void K3D_ModelTexturedFree(k3d_model_textured_t *model) {
        free(model->faces);
    }
    
    bool K3D_LoadModelTextured(k3d_model_textured_t *model, const char *const filepath, k3d_texture_set_t *textures) {
        typedef enum {
            OBJ, 
        } file_type_t;
        file_type_t file_type;
        if(strstr(filepath, "obj") != NULL) {
            file_type = OBJ;
        }
#define K3D_MAXVERTICES 512
        vec3_t vertices[K3D_MAXVERTICES];
        unsigned int vertex_index = 0;
#define K3D_MAXFACES 512
        k3d_obj_face_t faces[K3D_MAXFACES];
        unsigned int face_index = 0;
#define K3D_MAXUVS 512
        vec2_t uvs[K3D_MAXUVS];
        unsigned int uv_index = 0;
        unsigned int texture_indices[K3D_TEXTURE_COUNT];
        unsigned int texture_count = 0;
        switch(file_type) {
            case OBJ:{
                // Associated MTL file
                {
                    char *mtl_filename = malloc(64);
                    sprintf(mtl_filename, "%s", filepath);
                    sprintf(mtl_filename + strlen(mtl_filename) - 3, "mtl");
                    char *data;
                    uint32_t byte_count;
                    kstd_error_t error;
                    if((error = KStd_ReadFile(mtl_filename, (uint8_t**)&data, &byte_count)) != KSTD_SUCCESS) {
                        switch(error) {
                            case KSTD_ERROR_FILENOTFOUND:{
                                fprintf(stderr, "K3DM_Load() error: File not found: %s\n", mtl_filename);
                            }break;
                            case KSTD_ERROR_FILESEEK:{
                                fprintf(stderr, "K3DM_Load() error: fseek() failed: %s\n", mtl_filename);
                            }break;
                            case KSTD_ERROR_FILESIZE:{
                                fprintf(stderr, "K3DM_Load() error: File size invalid: %s\n", mtl_filename);
                            }break;
                            case KSTD_ERROR_MALLOC:{
                                fprintf(stderr, "K3DM_Load() error: Failed to allocate memory: %s\n", mtl_filename);
                            }break;
                            case KSTD_ERROR_FILEREAD:{
                                fprintf(stderr, "K3DM_Load() error: fread() failed: %s\n", mtl_filename);
                            }break;
                            default:{
                                fprintf(stderr, "K3DM_Load() error: Unknown error: %s\n", mtl_filename);
                            }break;
                        }
                        free(mtl_filename);
                        return false;
                    }
                    free(mtl_filename);
                    char c;
                    char *line;
                    char *line_next = data;
                    //bool last_line = false;
                    bool line_found = false;
                    char *float_next;
                    long l;
                    for(uint32_t i = 0; i < byte_count;) {
                        // New line
                        while(!line_found) {
                            c = data[i];
                            switch(c) {
                                case EOF:
                                case '\n':{
                                    data[i] = '\0';
                                    line = line_next;
                                    line_next = &data[i + 1];
                                    line_found = true;
                                }break;
                            }
                            ++i;
                        }
                        line_found = false;
                        switch(line[0]) {
                            case '#':{
#ifdef KERO_3D_VERBOSE
                                fprintf(stderr, "Comment: %s\n", &line[0]);
#endif
                            }break;
                            case 'n':{
#ifdef KERO_3D_VERBOSE
                                fprintf(stderr, "New material: %s\n", &line[0]);
#endif
                                if(line[1] == 'e' && line[2] == 'w' && line[3] == 'm' && line[4] == 't' && line[5] == 'l') {
                                    if(strlen(&line[7]) > 64) {
#ifdef KERO_3D_VERBOSE
                                        fprintf(stderr, "K3DM_Load(): Loading: %s. . . Material name longer than 64 characters: %s\n", filepath, &line[7]);
#endif
                                        free(data);
                                        return false;
                                    }
                                }
                            }break;
                            case 'm':{
                                if(line[1] == 'a' && line[2] == 'p' && line[3] == '_') {
                                    if(line[4] == 'K' && line[5] == 'd') { // Diffuse map
                                        /*if(!K3D_LoadTexture(&material_temp.texture, &line[7])) {
#ifdef KERO_3D_VERBOSE
                                            fprintf(stderr, "K3DM_Load(): Loading: %s. . . Failed to load texture: %s\n", filepath, &line[0]);
#endif
                                        }*/
                                        char name[K3D_TEXTURE_NAME_LENGTH+1] = {0};
                                        int name_start, name_end;
                                        name_end = strlen(line) - 4;
                                        name_start = name_end;
                                        while(line[name_start-1] != '/') --name_start;
                                        strncpy(name, &line[name_start], name_end - name_start);
                                        texture_indices[texture_count] = K3DTS_GetIndex(textures, name);
                                        if(texture_indices[texture_count] == -1) { texture_indices[texture_count] = 0;
                                            printf("Texture index[%d] invalid. Searching for %s\n", texture_count, name);
                                        }
                                        ++texture_count;
                                    }
                                    else if(line[4] == 'd') { // Opacity map
                                    }
                                }
                            }break;
                        }
                    }
                    free(data);
                }
                
                // OBJ file itself
                char *data;
                uint32_t byte_count;
                kstd_error_t error;
                if((error = KStd_ReadFile(filepath, (uint8_t**)&data, &byte_count)) != KSTD_SUCCESS) {
#ifdef KERO_3D_VERBOSE
                    switch(error) {
                        case KSTD_ERROR_FILENOTFOUND:{
                            fprintf(stderr, "K3DM_Load() error: File not found: %s\n", filepath);
                        }break;
                        case KSTD_ERROR_FILESEEK:{
                            fprintf(stderr, "K3DM_Load() error: fseek() failed: %s\n", filepath);
                        }break;
                        case KSTD_ERROR_FILESIZE:{
                            fprintf(stderr, "K3DM_Load() error: File size invalid: %s\n", filepath);
                        }break;
                        case KSTD_ERROR_MALLOC:{
                            fprintf(stderr, "K3DM_Load() error: Failed to allocate memory: %s\n", filepath);
                        }break;
                        case KSTD_ERROR_FILEREAD:{
                            fprintf(stderr, "K3DM_Load() error: fread() failed: %s\n", filepath);
                        }break;
                        default:{
                            fprintf(stderr, "K3DM_Load() error: Unknown error: %s\n", filepath);
                        }break;
                    }
#endif
                    return false;
                }
                char c;
                char *line;
                char *line_next = data;
                bool last_line = false;
                char *float_next;
                bool line_found = false;
                long l;
                vec3_t vertex_temp;
                vec2_t tex_coord_temp;
                k3d_obj_face_t face_temp = {0};
                for(int i = 0; i < byte_count && !last_line;) {
                    // New line
                    while(!line_found) {
                        c = data[i];
                        switch(c) {
                            case EOF:
                            case '\n':{
                                data[i] = '\0';
                                line = line_next;
                                line_next = &data[i + 1];
                                line_found = true;
                            }break;
                        }
                        ++i;
                    }
                    line_found = false;
                    switch(line[0]) {
                        case '#':{
#ifdef KERO_3D_VERBOSE
                            fprintf(stderr, "Comment: %s\n", &line[0]);
#endif
                        }break;
                        case 'v':{
#ifdef KERO_3D_VERBOSE
                            fprintf(stderr, "Vertex: %s\n", &line[0]);
#endif
                            switch(line[1]) {
                                case ' ':{
                                    float_next = &line[2];
                                    for(int i = 0; i < 3; ++i) {
                                        if((vertex_temp.V[i] = strtod(float_next, &float_next)) == HUGE_VAL || float_next == NULL ) {
#ifdef KERO_3D_VERBOSE
                                            fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read vertex.\n", filepath);
#endif
                                            free(data);
                                            return false;
                                        }
                                    }
                                    vertex_temp.z = -vertex_temp.z;
                                    vertices[vertex_index++] = vertex_temp;
                                }break;
                                case 't':{
                                    float_next = &line[2];
                                    for(int t = 0; t < 2; ++t) {
                                        if((tex_coord_temp.V[t] = strtod(float_next, &float_next)) == HUGE_VAL || float_next == NULL) {
#ifdef KERO_3D_VERBOSE
                                            fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read texture coordinates.\n", filepath);
#endif
                                            free(data);
                                            return false;
                                        }
                                    }
                                    uvs[uv_index++] = tex_coord_temp;
                                }break;
                                case 'p':{
                                }break;
                                case 'n': { // Vertex normal
                                    /*
                                    float_next = &line[3];
                                    for(int i = 0; i < 3; ++i) {
                                        if((vertex_temp.V[i] = strtod(float_next, &float_next)) == HUGE_VAL || float_next == NULL ) {
#ifdef KERO_3D_VERBOSE
                                            fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read vertex normal.\n", filepath);
#endif
                                            free(data);
                                            return false;
                                        }
                                    }
                                    if(KDA_Push(model->vn, vertex_temp) == NULL) {
#ifdef KERO_3D_VERBOSE
                                        fprintf(stderr, "K3DM_Load(): Loading: %s. . . Failed to realloc vertex normals\n", filepath);
#endif
                                        free(data);
                                        return false;
                                    }
*/
                                }break;
                            }
                        }break;
                        case 'f':{
#ifdef KERO_3D_VERBOSE
                            //fprintf(stderr, "Face: %s\n", &line[0]);
#endif
                            float_next = &line[1];
                            for(int i = 0; i < 3; ++i) {
                                face_temp.uv[i] = 0;
                                //face_temp.n[i] = 0;
                                if((l = strtol(float_next, &float_next, 10)) == LONG_MAX || l == LONG_MIN || float_next == NULL) {
#ifdef KERO_3D_VERBOSE
                                    fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read face.\n", filepath);
#endif
                                    free(data);
                                    return false;
                                }
                                if(l < 0) {
                                    face_temp.v[i] = vertex_index - (int)l;
                                }
                                else {
                                    face_temp.v[i] = (int)l - 1;
                                }
                                if(*float_next == '/') {
                                    ++float_next;
                                    // Texture coordinate index
                                    if(*float_next != '/') {
                                        if((l = strtol(float_next, &float_next, 10)) == LONG_MAX || l == LONG_MIN || float_next == NULL) {
#ifdef KERO_3D_VERBOSE
                                            fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read face.\n", filepath);
#endif
                                            free(data);
                                            return false;
                                        }
                                        face_temp.uv[i] = (uint32_t)(l - 1);
                                    }
                                }
                                if(*float_next == '/') {
                                    // Normal index
                                    
                                    if((l = strtol(float_next + 1, &float_next, 10)) == LONG_MAX || l == LONG_MIN) {
#ifdef KERO_3D_VERBOSE
                                        fprintf(stderr, "K3DM_Load(): Loading: %s. . . File format error: Failed to read face.\n", filepath);
#endif
                                        free(data);
                                        return false;
                                    }
                                    //face_temp.n[i] = (uint32_t)(l - 1);
                                    
                                }
                            }
                            faces[face_index++] = face_temp;
                        }break;
                        case 'u':{
                            if(line[1] == 's' && line[2] == 'e' && line[3] == 'm' && line[4] == 't' && line[5] == 'l') { // usemtl
#ifdef KERO_3D_VERBOSE
                                fprintf(stderr, "Material: %s\n", line);
#endif
                                // TODO: Get texture index from material (loaded earlier) rather than just using material name as texture name
                                int found = -1;
                                found = K3DTS_GetIndex(textures, &line[7]);
                                if(found != -1) {
                                    face_temp.t = found;
                                }
                                else {
                                    face_temp.t = 0;
                                }
                            }
                        }break;
                        case 'm':{
                            if(line[1] == 't' && line[2] == 'l' && line[3] == 'l' && line[4] == 'i' && line[5] == 'b') {
                            }
                        }break;
                    }
                }
                free(data);
                // Calculate face and vertex normals
                /*for(int i = 0; i < KDA_Top(model->faces); ++i) {
                    model->faces[i].normal = Vec3Cross(Vec3AToB(model->vertices[model->faces[i].v0], model->vertices[model->faces[i].v1]), Vec3AToB(model->vertices[model->faces[i].v0], model->vertices[model->faces[i].v2]));
                }
                uint32_t *vertex_faces;
                KDA_Init(vertex_faces, 64);
                float total_area;
                float *face_area;
                KDA_Init(face_area, 64);
                KDA_Init(model->vn, KDA_Top(model->vertices));
                for(int v = 0; v < KDA_Top(model->vertices); ++v) {
                    total_area = 0;
                    for(int f = 0; f < KDA_Top(model->faces); ++f) {
                        //fprintf(stderr, "%d ", f);
                        if(model->faces[f].v0 == v || model->faces[f].v1 == v || model->faces[f].v2 == v) {
                            KDA_Push(vertex_faces, f);
                            KDA_Push(face_area, Vec3Length(model->faces[f].normal));
                            total_area += face_area[KDA_Top(face_area) - 1];
                        }
                    }
                    model->vn[v] = Vec3Make(0, 0, 0);
                    for(int f = 0; f < KDA_Top(vertex_faces); ++f) {
                        model->vn[v].x += model->faces[vertex_faces[f]].n.x * face_area[f] / total_area;
                        model->vn[v].y += model->faces[vertex_faces[f]].n.y * face_area[f] / total_area;
                        model->vn[v].z += model->faces[vertex_faces[f]].n.z * face_area[f] / total_area;
                    }
                    model->vn[v] = Vec3Norm(model->vn[v]);
                    KDA_Top(vertex_faces) = 0;
                    KDA_Top(face_area) = 0;
                }
                KDA_Free(vertex_faces);
                for(int i = 0; i < KDA_Top(model->faces); ++i) {
                    model->faces[i].normal = Vec3Norm(model->faces[i].normal);
                }*/
            }break;
        }
        
        // Copy data from obj to k3d_model
        model->fc = face_index;
        model->f = (face_t *)malloc(face_index * sizeof(face_t));
        for(int f = 0; f < face_index; ++f) {
            for(int v = 0; v < 3; ++v) {
                model->f[f].v[v] = vertices[faces[f].v[2-v]];
                model->f[f].uv[v] = uvs[faces[f].uv[2-v]];
            }
            model->f[f].c = rand();
            model->f[f].t = faces[f].t;
            model->f[f].flags.double_sided = false;
        }
        
#ifdef KERO_3D_VERBOSE
        fprintf(stderr, "K3DM_Load(): Successfully loaded: %s\n", filepath);
#endif
        return true;
    }
    
    void K3D_ObjectSetModelTextured(k3d_object_t *object, k3d_model_textured_t *model) {
        object->fc = model->fc;
        object->f  = model->f;
    }
    
#ifdef __cplusplus
}
#endif

#define KERO_SOFTWARE_3D_H
#endif
